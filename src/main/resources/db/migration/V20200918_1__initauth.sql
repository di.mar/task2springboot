create table auth
(
	id serial not null,
	username varchar default 255 not null,
	email varchar default 255 not null,
	password varchar default 255 not null,
	role varchar default 255,
	forgot_password_key int default 128,
	forgot_password_key_timestamp bigint not null,
	company_unit_id bigint
);

create unique index auth_id_uindex
	on auth (id);

alter table auth
	add constraint auth_pk
		primary key (id);

create table users
(
	user_id serial not null,
	auth_id int not null,
	name varchar default 128 not null,
	fullname varchar default 128 not null,
	surname varchar default 128 not null,
	secondname varchar default 128 not null,
	status varchar default 128 not null,
	company_unit_id int not null,
	user_password varchar default 128,
	last_login_timestamp bigint not null,
	iin varchar default 32 not null,
	is_active bool not null,
	is_actived bool not null,
	created_timestamp bigint not null,
	created_by int not null,
	updated_timestamp bigint not null,
	updated_by int
);

create unique index users_user_id_uindex
	on users (user_id);

alter table users
	add constraint users_pk
		primary key (user_id);

create table company_unit
(
	company_unit_id serial not null,
	name_ru varchar default 128 not null,
	name_kz varchar default 128 not null,
	name_en varchar default 128 not null,
	parent_id int not null,
	company_year int not null,
	company_id int not null,
	created_timestamp bigint not null,
	created_by int not null,
	updated_timestamp bigint not null,
	updated_by int not null
);

create unique index company_unit_company_unit_id_uindex
	on company_unit (company_unit_id);

alter table company_unit
	add constraint company_unit_pk
		primary key (company_unit_id);

create table company
(
	company_id serial not null,
	name_ru varchar default 128 not null,
	name_kz varchar default 128 not null,
	name_en varchar default 128,
	bin varchar default 32 not null,
	parent_id int not null,
	fond_id int not null,
	created_timestamp bigint not null,
	created_by int not null,
	updated_timestamp bigint not null,
	updated_by int not null
);

create unique index company_company_id_uindex
	on company (company_id);

alter table company
	add constraint company_pk
		primary key (company_id);

create table fond
(
	fond_id int not null,
	fond_number varchar default 128 not null,
	created_timestamp bigint not null,
	created_by int not null,
	updated_timestamp bigint not null,
	updated_by int not null
);

create unique index fond_fond_id_uindex
	on fond (fond_id);

alter table fond
	add constraint fond_pk
		primary key (fond_id);

create table share
(
	share_id serial not null,
	request_id int not null,
	note varchar default 255 not null,
	sender_id int not null,
	receiver_id int not null,
	share_timestamp bigint
);

create unique index share_share_id_uindex
	on share (share_id);

alter table share
	add constraint share_pk
		primary key (share_id);

create table request_status_history
(
	id serial not null,
	request_id int not null,
	status int not null,
	created_timestamp bigint not null,
	created_by int not null,
	updated_timestamp bigint not null,
	updated_by int not null
);

create unique index request_status_history_id_uindex
	on request_status_history (id);

alter table request_status_history
	add constraint request_status_history_pk
		primary key (id);


create table request
(
	request_id serial not null,
	request_user_id int not null,
	response_user_id int not null,
	case_id int not null,
	case_index_id int not null,
	created_type varchar default 64 not null,
	comment varchar default 255,
	status varchar default 64 not null,
	timestamp bigint not null,
	sharestart int not null,
	sharefinish int not null,
	favorite bool not null,
	update_timestamp bigint not null,
	declinenote varchar default 255,
	company_unit_id int not null,
	from_request_id int
);

create unique index request_request_id_uindex
	on request (request_id);

alter table request
	add constraint request_pk
		primary key (request_id);

create table "cases"
(
	case_id serial not null,
	case_number varchar default 128 not null,
	tom_number varchar default 128 not null,
	heading_ru varchar default 128 not null,
	heading_kz varchar default 128 not null,
	heading_en varchar default 128 not null,
	start_date bigint not null,
	finish_date bigint,
	paper_number int not null,
	signed bool not null,
	sign text not null,
	send_NAF bool not null,
	deleted bool not null,
	available bool not null,
	hash varchar default 128,
	version int not null,
	version_id varchar default 128,
	active_version bool,
	note varchar default 255,
	location_id int,
	case_index_id int,
	record_id int,
	destroy_id int,
	company_unit_id int,
	blockchain_address int,
	blockchain_date int,
	created_timestamp bigint,
	created_by int,
	updated_timestamp bigint,
	updated_by int
);

create unique index cases_case_id_uindex
	on "cases" (case_id);

alter table "cases"
	add constraint case_pk
		primary key (case_id);

create table nomenclature
(
	nomenclature_id serial not null,
	nomenclature_number varchar default 128,
	nomenclature_year int,
	nomenclature_summary_id int,
	company_unit_id int,
	column_6 int,
	created_timestamp bigint,
	created_by int,
	updated_timestamp bigint,
	updated_by int
);

create unique index nomenclature_nomenclature_id_uindex
	on nomenclature (nomenclature_id);

alter table nomenclature
	add constraint nomenclature_pk
		primary key (nomenclature_id);

create table nomenclature_summary
(
	nomenclature_summary_id serial not null,
	number varchar default 128,
	summary_year int,
	company_unit_id int,
	created_timestamp bigint,
	created_by int,
	updated_timestamp bigint,
	updated_by int
);

create unique index nomenclature_summary_nomenclature_summary_id_uindex
	on nomenclature_summary (nomenclature_summary_id);

alter table nomenclature_summary
	add constraint nomenclature_summary_pk
		primary key (nomenclature_summary_id);

create table case_index
(
	case_index_id serial not null,
	case_index varchar default 128,
	title_ru varchar default 128,
	title_kz varchar default 128,
	title_en varchar default 128,
	storage_type int,
	storage_year int,
	note varchar default 128,
	company_unit_id int,
	nomenclature_id int,
	created_timestamp bigint,
	created_by int,
	updated_timestamp bigint,
	updated_by int
);

create unique index case_index_case_index_id_uindex
	on case_index (case_index_id);

alter table case_index
	add constraint case_index_pk
		primary key (case_index_id);

create table destroy
(
	destroy_id serial not null,
	destroy_number varchar default 128,
	founded varchar default 256,
	company_unit_id int,
	created_timestamp bigint,
	created_by int,
	updated_timestamp bigint,
	updated_by int
);

create unique index destroy_destroy_id_uindex
	on destroy (destroy_id);

alter table destroy
	add constraint destroy_pk
		primary key (destroy_id);

create table record
(
	record_id serial not null,
	record_number varchar default 128,
	record_type varchar default 128,
	company_unit_id int,
	created_timestamp bigint,
	created_by int,
	updated_timestamp bigint,
	updated_by int
);

create unique index record_record_id_uindex
	on record (record_id);

alter table record
	add constraint record_pk
		primary key (record_id);

create table catalog_case
(
	catalog_case_id serial not null,
	case_id int,
	catalog_id int,
	company_unit_id int,
	created_timestamp bigint,
	created_by int,
	updated_timestamp bigint,
	updated_by int
);

create unique index catalog_case_catalog_case_id_uindex
	on catalog_case (catalog_case_id);

alter table catalog_case
	add constraint catalog_case_pk
		primary key (catalog_case_id);

create table catalog
(
	catalog_id serial not null,
	name_ru varchar default 128,
	ame_kz varchar default 128,
	name_en varchar default 128,
	parent_id int,
	company_unit_id int,
	created_timestamp bigint,
	created_by int,
	updated_timestamp bigint,
	updated_by int
);

create unique index catalog_catalog_id_uindex
	on catalog (catalog_id);

alter table catalog
	add constraint catalog_pk
		primary key (catalog_id);

create table location
(
	location_id serial not null,
	row varchar default 64,
	line varchar default 64,
	"column" varchar default 64,
	box varchar default 64,
	company_unit_id int,
	created_timestamp bigint,
	created_by int,
	updated_timestamp bigint,
	updated_by int
);

create unique index location_location_id_uindex
	on location (location_id);

alter table location
	add constraint location_pk
		primary key (location_id);

create table activity_journal
(
	activity_journal_id serial not null,
	activity_type varchar default 128,
	object_type varchar default 255,
	object_id int,
	created_timestamp bigint,
	created_by int,
	updated_timestamp bigint,
	updated_by int
);

create unique index activity_journal_activity_journal_id_uindex
	on activity_journal (activity_journal_id);

alter table activity_journal
	add constraint activity_journal_pk
		primary key (activity_journal_id);

create table notification
(
	notification_id serial not null,
	object_type varchar default 128,
	object_id int,
	company_unit_id int,
	user_id int,
	created_timestamp bigint,
	viewed_timestamp bigint,
	is_viewed bool,
	title varchar default 255,
	text varchar default 255,
	company_id int
);

create unique index notification_notification_id_uindex
	on notification (notification_id);

alter table notification
	add constraint notification_pk
		primary key (notification_id);

create table file
(
	file_id serial not null,
	name varchar default 128,
	type varchar default 128,
	size int,
	page_count int,
	hash varchar default 128,
	is_deleted bool,
	file_binary_id int,
	created_timestamp bigint,
	created_by int,
	updated_timestamp bigint,
	updated_by int
);

create unique index file_file_id_uindex
	on file (file_id);

alter table file
	add constraint file_pk
		primary key (file_id);

create table tempfiles
(
	file_binary_id serial not null,
	file_binary text,
	file_binary_byte bytea
);

create unique index tempfiles_file_binary_id_uindex
	on tempfiles (file_binary_id);

alter table tempfiles
	add constraint tempfiles_pk
		primary key (file_binary_id);

create table file_routing
(
	file_routing_id serial not null,
	file_id int,
	table_name varchar default 128,
	case_id int,
	type varchar default 128
);

create unique index file_routing_file_routing_id_uindex
	on file_routing (file_routing_id);

alter table file_routing
	add constraint file_routing_pk
		primary key (file_routing_id);

create table searchkey
(
	searchkey_id serial not null,
	name varchar default 128,
	company_unit_id int,
	created_timestamp bigint,
	created_by int,
	updated_timestamp bigint,
	updated_by int
);

create unique index searchkey_searchkey_id_uindex
	on searchkey (searchkey_id);

alter table searchkey
	add constraint searchkey_pk
		primary key (searchkey_id);


create table search_key_routing
(
	skrouting_id serial not null,
	search_key_id int,
	table_name varchar default 128,
	case_id int,
	type varchar default 128
);

create unique index search_key_routing_skrouting_id_uindex
	on search_key_routing (skrouting_id);

alter table search_key_routing
	add constraint search_key_routing_pk
		primary key (skrouting_id);

ALTER TABLE auth
ADD FOREIGN KEY (company_unit_id) REFERENCES company_unit(company_unit_id);

ALTER TABLE users
ADD FOREIGN KEY (auth_id) REFERENCES auth(id);
ALTER TABLE users
ADD FOREIGN KEY (company_unit_id) REFERENCES company_unit(company_unit_id);

ALTER TABLE company_unit
ADD FOREIGN KEY (company_id) REFERENCES company(company_id);

ALTER TABLE company
ADD FOREIGN KEY (fond_id) REFERENCES fond(fond_id);

ALTER TABLE share
ADD FOREIGN KEY (request_id) REFERENCES request(request_id);

ALTER TABLE request_status_history
ADD FOREIGN KEY (request_id) REFERENCES request(request_id);

ALTER TABLE request
ADD FOREIGN KEY (case_id) REFERENCES cases(case_id);
ALTER TABLE request
ADD FOREIGN KEY (case_index_id) REFERENCES case_index(case_index_id);
ALTER TABLE request
ADD FOREIGN KEY (company_unit_id) REFERENCES company_unit(company_unit_id);

ALTER TABLE cases
ADD FOREIGN KEY (case_index_id) REFERENCES case_index(case_index_id);
ALTER TABLE cases
ADD FOREIGN KEY (location_id) REFERENCES location(location_id);
ALTER TABLE cases
ADD FOREIGN KEY (record_id) REFERENCES record(record_id);
ALTER TABLE cases
ADD FOREIGN KEY (destroy_id) REFERENCES destroy(destroy_id);
ALTER TABLE cases
ADD FOREIGN KEY (company_unit_id) REFERENCES company_unit(company_unit_id);

ALTER TABLE nomenclature
ADD FOREIGN KEY (nomenclature_summary_id) REFERENCES nomenclature_summary(nomenclature_summary_id);

ALTER TABLE nomenclature_summary
ADD FOREIGN KEY (company_unit_id) REFERENCES company_unit(company_unit_id);

ALTER TABLE case_index
ADD FOREIGN KEY (company_unit_id) REFERENCES company_unit(company_unit_id);
ALTER TABLE case_index
ADD FOREIGN KEY (nomenclature_id) REFERENCES nomenclature(nomenclature_id);

ALTER TABLE destroy
ADD FOREIGN KEY (company_unit_id) REFERENCES company_unit(company_unit_id);

ALTER TABLE record
ADD FOREIGN KEY (company_unit_id) REFERENCES company_unit(company_unit_id);

ALTER TABLE catalog
ADD FOREIGN KEY (company_unit_id) REFERENCES company_unit(company_unit_id);

ALTER TABLE catalog_case
ADD FOREIGN KEY (catalog_id) REFERENCES catalog(catalog_id);
ALTER TABLE catalog_case
ADD FOREIGN KEY (company_unit_id) REFERENCES company_unit(company_unit_id);

ALTER TABLE location
ADD FOREIGN KEY (company_unit_id) REFERENCES company_unit(company_unit_id);

ALTER TABLE notification
ADD FOREIGN KEY (company_unit_id) REFERENCES company_unit(company_unit_id);
ALTER TABLE notification
ADD FOREIGN KEY (company_id) REFERENCES company(company_id);
ALTER TABLE notification
ADD FOREIGN KEY (user_id) REFERENCES users(user_id);

ALTER TABLE file
ADD FOREIGN KEY (file_binary_id) REFERENCES tempfiles(file_binary_id);

ALTER TABLE file_routing
ADD FOREIGN KEY (file_id) REFERENCES file(file_id);
ALTER TABLE file_routing
ADD FOREIGN KEY (case_id) REFERENCES cases(case_id);

ALTER TABLE searchkey
ADD FOREIGN KEY (company_unit_id) REFERENCES company_unit(company_unit_id);

ALTER TABLE search_key_routing
ADD FOREIGN KEY (search_key_id) REFERENCES searchkey(searchkey_id);
ALTER TABLE search_key_routing
ADD FOREIGN KEY (case_id) REFERENCES cases(case_id);
