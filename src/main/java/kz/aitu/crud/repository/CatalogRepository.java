package kz.aitu.crud.repository;

import kz.aitu.crud.model.Catalog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CatalogRepository extends CrudRepository<Catalog, Long> {
Catalog findById(long id);
}
