package kz.aitu.crud.repository;

import kz.aitu.crud.model.File;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepository extends CrudRepository<File, Long> {
    File findById(long id);
}
