package kz.aitu.crud.repository;

import kz.aitu.crud.model.FileRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRoutingRepository extends CrudRepository<FileRouting, Long> {
FileRouting findById(long id);
}
