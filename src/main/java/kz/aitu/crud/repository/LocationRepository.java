package kz.aitu.crud.repository;

import kz.aitu.crud.model.Location;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends CrudRepository<Location, Long> {
    Location findById(long id);
}
