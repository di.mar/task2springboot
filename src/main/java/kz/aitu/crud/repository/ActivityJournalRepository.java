package kz.aitu.crud.repository;

import kz.aitu.crud.model.ActivityJournal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityJournalRepository extends CrudRepository<ActivityJournal, Long> {
    ActivityJournal findById(long id);
}
