package kz.aitu.crud.repository;

import kz.aitu.crud.model.Tempfiles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TempfilesRepository extends CrudRepository<Tempfiles,Long> {
    Tempfiles findById(long id);
}
