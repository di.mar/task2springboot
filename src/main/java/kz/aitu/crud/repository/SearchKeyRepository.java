package kz.aitu.crud.repository;

import kz.aitu.crud.model.SearchKey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchKeyRepository extends CrudRepository<SearchKey,Long> {
    SearchKey findById(long id);
}
