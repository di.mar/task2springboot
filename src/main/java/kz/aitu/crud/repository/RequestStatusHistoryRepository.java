package kz.aitu.crud.repository;

import kz.aitu.crud.model.RequestStatusHistory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestStatusHistoryRepository extends CrudRepository<RequestStatusHistory,Long> {
    RequestStatusHistory findById(long id);
}
