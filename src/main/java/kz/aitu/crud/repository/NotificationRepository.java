package kz.aitu.crud.repository;

import kz.aitu.crud.model.Nomenclature;
import kz.aitu.crud.model.Notification;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationRepository extends CrudRepository<Notification, Long> {
    Notification findById(long id);
}
