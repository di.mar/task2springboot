package kz.aitu.crud.repository;

import kz.aitu.crud.model.Fond;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FondRepository extends CrudRepository<Fond, Long> {
    Fond findById(long id);
}
