package kz.aitu.crud.repository;

import kz.aitu.crud.model.SearchKeyRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchKeyRoutingRepository  extends CrudRepository<SearchKeyRouting,Long> {
    SearchKeyRouting findById(long id);
}
