package kz.aitu.crud.repository;

import kz.aitu.crud.model.Nomenclature;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NomenclatureRepository extends CrudRepository<Nomenclature, Long> {
    Nomenclature findById(long id);
}
