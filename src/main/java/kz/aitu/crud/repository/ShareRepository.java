package kz.aitu.crud.repository;


import kz.aitu.crud.model.Share;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShareRepository extends CrudRepository<Share,Long> {
 Share findById(long id);
}
