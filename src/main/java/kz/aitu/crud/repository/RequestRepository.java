package kz.aitu.crud.repository;

import kz.aitu.crud.model.Request;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestRepository extends CrudRepository<Request,Long> {
    Request findById(long id);
}
