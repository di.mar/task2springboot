package kz.aitu.crud.repository;

import kz.aitu.crud.model.CompanyUnit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyUnitRepository extends CrudRepository<CompanyUnit, Long> {
CompanyUnit findById(long id);
}
