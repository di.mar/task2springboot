package kz.aitu.crud.repository;

import kz.aitu.crud.model.Destroy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DestroyRepository extends CrudRepository<Destroy, Long> {
    Destroy findById(long id);
}
