package kz.aitu.crud.repository;

import kz.aitu.crud.model.Record;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecordRepository extends CrudRepository<Record,Long> {
    Record findById(long id);
}
