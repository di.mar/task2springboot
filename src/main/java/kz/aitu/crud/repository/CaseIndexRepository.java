package kz.aitu.crud.repository;

import kz.aitu.crud.model.CaseIndex;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaseIndexRepository extends CrudRepository<CaseIndex, Long> {
    CaseIndex findById(long id);
}
