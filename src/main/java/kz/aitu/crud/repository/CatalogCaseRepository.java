package kz.aitu.crud.repository;

import kz.aitu.crud.model.CatalogCase;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CatalogCaseRepository extends CrudRepository<CatalogCase, Long> {
    CatalogCase findById(long id);
}
