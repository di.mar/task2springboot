package kz.aitu.crud.repository;

import kz.aitu.crud.model.Case;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaseRepository extends CrudRepository<Case, Long> {
Case findById(long id);
}
