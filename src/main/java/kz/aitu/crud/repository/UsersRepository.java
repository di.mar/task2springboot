package kz.aitu.crud.repository;

import kz.aitu.crud.model.Users;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends CrudRepository<Users, Long> {
    Users findById(long id);
}
