package kz.aitu.crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name ="case_index")
public class CaseIndex {
    @Id
    private long caseIndexId;
    private String caseIndex;
    private String titleRu;
    private String titleKz;
    private String titleEn;
    private int storageType;
    private int storageYear;
    private String note;
    private long companyUnitId;
    private long nomenclatureId;
    private long createdTimestamp;
    private int createdBy;
    private long updatedTimestamp;
    private int updatedBy;
}
