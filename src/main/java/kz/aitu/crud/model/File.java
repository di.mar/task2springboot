package kz.aitu.crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name ="file")
public class File {
    @Id
    private long fileId;
    private String fileName;
    private String fileType;
    private int fileSize;
    private int pageCount;
    private String hash;
    private boolean isDeleated;
    private long fileBinaryId;
    private long createdTimestamp;
    private int createdBy;
    private long updatedTimestamp;
    private int updatedBy;
}
