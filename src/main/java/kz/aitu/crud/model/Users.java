package kz.aitu.crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class Users {
    @Id
    private long userId;
    private long authId;
    private String name;
    private String fullname;
    private String surname;
    private String status;
    private long companyUnitId;
    private String password;
    private long lastLoginTimestamp;
    private long iin;
    private boolean isActive;
    private boolean isActivated;
    private long createdTimestamp;
    private int createdBy;
    private long updatedTimestamp;
    private int updatedBy;


}
