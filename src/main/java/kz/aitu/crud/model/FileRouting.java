package kz.aitu.crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name ="file_routing")
public class FileRouting {
    @Id
    private long fileRoutingId;
    private long fileId;
    private String tableName;
    private long caseId;
    private String type;
}
