package kz.aitu.crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="request")
public class Request {
    @Id
    private long requestId;
    private int requestUserId;
    private int responseUserId;
    private long caseId;
    private long caseIndexId;
    private String created_type;
    private String comment;
    private String status;
    private int timestamp;
    private int sharestart;
    private int sharefinish;
    private boolean favorite;
    private int updateTimestamp;
    private String declinenote;
    private long companyUnitId;
    private int fromRequestId;
}
