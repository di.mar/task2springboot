package kz.aitu.crud.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name ="company")
public class Company {
    @Id
    private long companyId;
    private String nameRu;
    private String nameKz;
    private String nameEn;
    private long bin;
    private int parentId;
    private long fondId;
    private long createdTimestamp;
    private int createdBy;
    private long updatedTimestamp;
    private int updatedBy;
}
