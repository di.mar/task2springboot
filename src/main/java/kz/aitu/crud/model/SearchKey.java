package kz.aitu.crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="searchkey")
public class SearchKey {
    @Id
    private long searchkeyId;
    private String name;
    private long companyUnitId;
    private long createdTimestamp;
    private int createdBy;
    private long updatedTimestamp;
    private int updatedBy;
}
