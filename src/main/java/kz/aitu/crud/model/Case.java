package kz.aitu.crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="case")
public class Case {
    @Id
    private long caseId;
    private int caseNumber;
    private int tomNumber;
    private String headingRu;
    private String headingKz;
    private String headingEn;
    private long startDate;
    private long finishDate;
    private int paperNumber;
    private boolean signed;
    private String sign;
    private boolean sendNAF;
    private boolean deleted;
    private boolean available;
    private String hash;
    private int version;
    private String versionId;
    private boolean activeVersion;
    private String varchar;
    private int locationId;
    private int caseIndexId;
    private int signId;
    private int destroyId;
    private int companyUnitId;
    private int blockchainAddress;
    private int blockchainDate;
    private long createdTimestamp;
    private int createdBy;
    private long updatedTimestamp;
    private int updatedBy;
}
