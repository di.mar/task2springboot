package kz.aitu.crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="destroy")
public class Destroy {
    @Id
    private long destroyId;
    private String destroyNumber;
    private String founded;
    private long CompanyUnitId;
    private long createdTimestamp;
    private int createdBy;
    private long updatedTimestamp;
    private int updatedBy;
}
