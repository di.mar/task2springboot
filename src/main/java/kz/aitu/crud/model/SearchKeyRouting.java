package kz.aitu.crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="search_key_routing")
public class SearchKeyRouting {
    @Id
    private long skRouting;
    private long searchkeyId;
    private String tableName;
    private long caseId;
    private String type;
}
