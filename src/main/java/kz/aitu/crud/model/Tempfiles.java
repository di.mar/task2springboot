package kz.aitu.crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="tempfiles")
public class Tempfiles {
    @Id
    private long fileBinaryId;
    private String fileBinary;
    private byte fileBinaryByte;
}
