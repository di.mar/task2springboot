package kz.aitu.crud.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "company_unit")
public class CompanyUnit {
    @Id
    private long companyUnitId;
    private String nameRu;
    private String nameKz;
    private String nameEn;
    private int parentId;
    private int companyYear;
    private long companyId;
    private String codeIndex;
    private long createdTimestamp;
    private int createdBy;
    private long updatedTimestamp;
    private int updatedBy;

}
