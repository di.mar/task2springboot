package kz.aitu.crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="nomenclature_summary")
public class NomenclatureSummary {
    @Id
    private long nomenclatureSummaryId;
    private int nomenclatureSummaryNumber;
    private int year;
    private long companyUnitId;
    private long createdTimestamp;
    private int createdBy;
    private long updatedTimestamp;
    private int updatedBy;
}
