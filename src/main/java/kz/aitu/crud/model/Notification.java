package kz.aitu.crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="notification")
public class Notification {
    @Id
    private long notificationId;
    private String objectType;
    private int objectId;
    private long companyUnitId;
    private int userId;
    private long createrTimestamp;
    private long viewedTimestamp;
    private boolean isViewed;
    private String title;
    private String text;
    private long companyId;
}
