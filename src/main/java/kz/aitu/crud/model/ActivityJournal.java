package kz.aitu.crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="activity_journal")
public class ActivityJournal {
    @Id
    private long activityJournalId;
    private String activtyType;
    private String objectType;
    private int objectId;
    private long createdTimestamp;
    private int createdBy;
    private String message_level;
    private String message;
}
