package kz.aitu.crud.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="share")
public class Share {
    @Id
    private long share_id;
    private long request_id;
    private String note;
    private int senderId;
    private int receiverId;
    private long share_timestamp;

}
