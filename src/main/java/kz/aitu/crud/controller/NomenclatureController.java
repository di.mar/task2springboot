package kz.aitu.crud.controller;

import kz.aitu.crud.model.Location;
import kz.aitu.crud.model.Nomenclature;
import kz.aitu.crud.service.NomenclatureService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NomenclatureController {
    private NomenclatureService nomenclatureService;

    public NomenclatureController(NomenclatureService nomenclatureService) {
        this.nomenclatureService = nomenclatureService;
    }

    @GetMapping("/getNomenclatureRouting")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(nomenclatureService.getAll());
    }

    @GetMapping("/getNomenclature/{id}")
    public ResponseEntity<?> getNomenclatureById(@PathVariable int id){
        return ResponseEntity.ok(nomenclatureService.getNomenclatureById(id));
    }

    @DeleteMapping("/deleteNomenclature/{id}")
    public void deleteNomenclature(@PathVariable long id){
        nomenclatureService.deleteById(id);
    }

    @PostMapping("/addNomenclature")
    public ResponseEntity<?> createNomenclature(@RequestBody Nomenclature nomenclature){
        return ResponseEntity.ok(nomenclatureService.createNomenclature(nomenclature));
    }
}
