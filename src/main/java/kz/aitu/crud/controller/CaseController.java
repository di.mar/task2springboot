package kz.aitu.crud.controller;

import kz.aitu.crud.model.Case;
import kz.aitu.crud.service.CaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CaseController {
    private CaseService caseService;

    public CaseController(CaseService caseService) {
        this.caseService = caseService;
    }

    @GetMapping("/getCase")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(caseService.getAll());
    }

    @GetMapping("/getCase/{id}")
    public ResponseEntity<?> getCaseById(@PathVariable int id){
        return ResponseEntity.ok(caseService.getCaseById(id));
    }

    @DeleteMapping("/deleteCase/{id}")
    public void deleteCase(@PathVariable long id){
        caseService.deleteById(id);
    }

    @PostMapping("/addCase")
    public ResponseEntity<?> createCase(@RequestBody Case cased){
        return ResponseEntity.ok(caseService.createCase(cased));
    }

}
