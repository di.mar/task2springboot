package kz.aitu.crud.controller;

import kz.aitu.crud.model.Record;
import kz.aitu.crud.service.RecordService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RecordController {
    private RecordService recordService;

    public RecordController(RecordService recordService) {
        this.recordService = recordService;
    }

    @GetMapping("/getRecord")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(recordService.getAll());
    }

    @GetMapping("/getRecord/{id}")
    public ResponseEntity<?> getRecordById(@PathVariable int id){
        return ResponseEntity.ok(recordService.getRecordById(id));
    }

    @DeleteMapping("/deleteRecord/{id}")
    public void deleteRecord(@PathVariable long id){
        recordService.deleteById(id);
    }

    @PostMapping("/addRecord")
    public ResponseEntity<?> createRecord(@RequestBody Record record){
        return ResponseEntity.ok(recordService.createRecord(record));
    }
}
