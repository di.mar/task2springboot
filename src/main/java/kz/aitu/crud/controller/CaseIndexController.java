package kz.aitu.crud.controller;

import kz.aitu.crud.model.ActivityJournal;
import kz.aitu.crud.model.CaseIndex;
import kz.aitu.crud.service.ActivityJournalService;
import kz.aitu.crud.service.CaseIndexService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class CaseIndexController {
    private CaseIndexService caseIndexService;

    public CaseIndexController(CaseIndexService caseIndexService) {
        this.caseIndexService = caseIndexService;
    }

    @GetMapping("/getCaseIndex")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(caseIndexService.getAll());
    }

    @GetMapping("/getCaseIndex/{id}")
    public ResponseEntity<?> getCaseIndexById(@PathVariable int id){
        return ResponseEntity.ok(caseIndexService.getCaseIndexById(id));
    }

    @DeleteMapping("/deleteCaseIndex/{id}")
    public void deleteCaseIndex(@PathVariable long id){
        caseIndexService.deleteById(id);
    }

    @PostMapping("/addCaseIndex")
    public ResponseEntity<?> createCaseIndex(@RequestBody CaseIndex caseIndex){
        return ResponseEntity.ok(caseIndexService.createCaseIndex(caseIndex));
    }
}
