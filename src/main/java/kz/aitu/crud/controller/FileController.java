package kz.aitu.crud.controller;

import kz.aitu.crud.model.File;
import kz.aitu.crud.model.FileRouting;
import kz.aitu.crud.service.FileRoutingService;
import kz.aitu.crud.service.FileService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FileController {
    private FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping("/getFile")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(fileService.getAll());
    }

    @GetMapping("/getFile/{id}")
    public ResponseEntity<?> getFileById(@PathVariable int id){
        return ResponseEntity.ok(fileService.getFileById(id));
    }

    @DeleteMapping("/deleteFile/{id}")
    public void deleteFile(@PathVariable long id){
        fileService.deleteById(id);
    }

    @PostMapping("/addFile")
    public ResponseEntity<?> createFile(@RequestBody File file){
        return ResponseEntity.ok(fileService.createFile(file));
    }
}
