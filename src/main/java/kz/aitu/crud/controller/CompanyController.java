package kz.aitu.crud.controller;

import kz.aitu.crud.model.Catalog;
import kz.aitu.crud.model.Company;
import kz.aitu.crud.service.CatalogService;
import kz.aitu.crud.service.CompanyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyController {
    private CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping("/getCompany")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(companyService.getAll());
    }

    @GetMapping("/getCompany/{id}")
    public ResponseEntity<?> getCompanyById(@PathVariable int id){
        return ResponseEntity.ok(companyService.getCompanyById(id));
    }

    @DeleteMapping("/deleteCompany/{id}")
    public void deleteCompany(@PathVariable long id){
        companyService.deleteById(id);
    }

    @PostMapping("/addCompany")
    public ResponseEntity<?> createCompany(@RequestBody Company company){
        return ResponseEntity.ok(companyService.createCompany(company));
    }
}
