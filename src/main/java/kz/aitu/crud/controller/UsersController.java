package kz.aitu.crud.controller;

import kz.aitu.crud.model.Tempfiles;
import kz.aitu.crud.model.Users;
import kz.aitu.crud.service.UsersService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UsersController {
    private UsersService usersService;

    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping("/getUsers")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(usersService.getAll());
    }

    @GetMapping("/getUsers/{id}")
    public ResponseEntity<?> getUsersById(@PathVariable int id){
        return ResponseEntity.ok(usersService.getUsersById(id));
    }

    @DeleteMapping("/deleteUsers/{id}")
    public void deleteUsers(@PathVariable long id){
        usersService.deleteById(id);
    }

    @PostMapping("/addUsers")
    public ResponseEntity<?> createUsers(@RequestBody Users users){
        return ResponseEntity.ok(usersService.createUsers(users));
    }
}
