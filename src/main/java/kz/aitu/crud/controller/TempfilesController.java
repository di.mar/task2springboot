package kz.aitu.crud.controller;

import kz.aitu.crud.model.Share;
import kz.aitu.crud.model.Tempfiles;
import kz.aitu.crud.service.TempfilesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TempfilesController {
    private TempfilesService tempfilesService;

    public TempfilesController(TempfilesService tempfilesService) {
        this.tempfilesService = tempfilesService;
    }

    @GetMapping("/getTempfiles")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(tempfilesService.getAll());
    }

    @GetMapping("/getTempfiles/{id}")
    public ResponseEntity<?> getTempfilesById(@PathVariable int id){
        return ResponseEntity.ok(tempfilesService.getTempfilesById(id));
    }

    @DeleteMapping("/deleteTempfiles/{id}")
    public void deleteTempfiles(@PathVariable long id){
        tempfilesService.deleteById(id);
    }

    @PostMapping("/addTempfiles")
    public ResponseEntity<?> createTempfiles(@RequestBody Tempfiles tempfiles){
        return ResponseEntity.ok(tempfilesService.createTempfiles(tempfiles));
    }
}
