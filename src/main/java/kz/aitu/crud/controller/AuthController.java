package kz.aitu.crud.controller;


import kz.aitu.crud.model.Auth;
import kz.aitu.crud.service.AuthService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthController {
    private AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @GetMapping("/getAuth")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(authService.getAll());
    }

    @GetMapping("/getAuth/{id}")
    public ResponseEntity<?> getAuthById(@PathVariable int id){
        return ResponseEntity.ok(authService.getAuthById(id));
    }

    @DeleteMapping("/deleteAuth/{id}")
    public void deleteAuth(@PathVariable long id){
        authService.deleteById(id);
    }

    @PostMapping("/addAuth")
    public ResponseEntity<?> createAuth(@RequestBody Auth auth){
        return ResponseEntity.ok(authService.createAuth(auth));
    }

    @PostMapping("/updateAuth/{id}")
    public ResponseEntity<?> updateAuth(@PathVariable long id, @RequestBody Auth auth){
        return ResponseEntity.ok(authService.createAuth(auth));
    }


}
