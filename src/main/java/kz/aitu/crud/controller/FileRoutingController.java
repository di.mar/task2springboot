package kz.aitu.crud.controller;

import kz.aitu.crud.model.CompanyUnit;
import kz.aitu.crud.model.FileRouting;
import kz.aitu.crud.service.CompanyUnitService;
import kz.aitu.crud.service.FileRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FileRoutingController {
    private FileRoutingService fileRoutingService;

    public FileRoutingController(FileRoutingService fileRoutingService) {
        this.fileRoutingService = fileRoutingService;
    }

    @GetMapping("/getFileRouting")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(fileRoutingService.getAll());
    }

    @GetMapping("/getFileRouting/{id}")
    public ResponseEntity<?> getFileRoutingById(@PathVariable int id){
        return ResponseEntity.ok(fileRoutingService.getFileRoutingById(id));
    }

    @DeleteMapping("/deleteFileRouting/{id}")
    public void deleteFileRouting(@PathVariable long id){
        fileRoutingService.deleteById(id);
    }

    @PostMapping("/addFileRouting")
    public ResponseEntity<?> createFileRouting(@RequestBody FileRouting fileRouting){
        return ResponseEntity.ok(fileRoutingService.createFileRouting(fileRouting));
    }
}
