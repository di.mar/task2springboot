package kz.aitu.crud.controller;

import kz.aitu.crud.model.Request;
import kz.aitu.crud.model.RequestStatusHistory;
import kz.aitu.crud.service.RequestStatusHistoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RequestStatusHistoryController {
    private RequestStatusHistoryService requestStatusHistoryService;

    public RequestStatusHistoryController(RequestStatusHistoryService requestStatusHistoryService) {
        this.requestStatusHistoryService = requestStatusHistoryService;
    }
    @GetMapping("/getRequestStatusHistory")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(requestStatusHistoryService.getAll());
    }

    @GetMapping("/getRecordStatusHistory/{id}")
    public ResponseEntity<?> getRequestStatusHistoryById(@PathVariable int id){
        return ResponseEntity.ok(requestStatusHistoryService.getRequestStatusById(id));
    }

    @DeleteMapping("/deleteRequestStatusHistory/{id}")
    public void deleteRequestStatusHistory(@PathVariable long id){
        requestStatusHistoryService.deleteById(id);
    }

    @PostMapping("/addRequestStatusHistory")
    public ResponseEntity<?> createRecordStatusHistory(@RequestBody RequestStatusHistory requestStatusHistory){
        return ResponseEntity.ok(requestStatusHistoryService.createRequestStatus(requestStatusHistory));
    }
}
