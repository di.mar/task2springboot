package kz.aitu.crud.controller;

import kz.aitu.crud.model.CompanyUnit;
import kz.aitu.crud.model.Destroy;
import kz.aitu.crud.service.DestroyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class DestroyController {
    private DestroyService destroyService;

    public DestroyController(DestroyService destroyService) {
        this.destroyService = destroyService;
    }

    @GetMapping("/getDestroy")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(destroyService.getAll());
    }

    @GetMapping("/getDestroy/{id}")
    public ResponseEntity<?> getDestroyById(@PathVariable int id){
        return ResponseEntity.ok(destroyService.getDestroyById(id));
    }

    @DeleteMapping("/deleteDestroy/{id}")
    public void deleteDestroy(@PathVariable long id){
        destroyService.deleteById(id);
    }

    @PostMapping("/addDestroy")
    public ResponseEntity<?> createDestroy(@RequestBody Destroy destroy){
        return ResponseEntity.ok(destroyService.createDestroy(destroy));
    }
}
