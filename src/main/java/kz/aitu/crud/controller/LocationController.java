package kz.aitu.crud.controller;

import kz.aitu.crud.model.FileRouting;
import kz.aitu.crud.model.Location;
import kz.aitu.crud.service.LocationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LocationController {
    private LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }
    @GetMapping("/getLocationRouting")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(locationService.getAll());
    }

    @GetMapping("/getLocation/{id}")
    public ResponseEntity<?> getLocationById(@PathVariable int id){
        return ResponseEntity.ok(locationService.getLocationById(id));
    }

    @DeleteMapping("/deleteLocation/{id}")
    public void deleteLocation(@PathVariable long id){
        locationService.deleteById(id);
    }

    @PostMapping("/addLocation")
    public ResponseEntity<?> createLocation(@RequestBody Location location){
        return ResponseEntity.ok(locationService.createLocation(location));
    }
}
