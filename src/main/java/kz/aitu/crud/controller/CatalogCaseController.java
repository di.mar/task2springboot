package kz.aitu.crud.controller;

import kz.aitu.crud.model.Case;
import kz.aitu.crud.model.CatalogCase;
import kz.aitu.crud.service.CaseService;
import kz.aitu.crud.service.CatalogCaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class CatalogCaseController {
    private CatalogCaseService catalogCaseService;

    public CatalogCaseController(CatalogCaseService catalogCaseService) {
        this.catalogCaseService = catalogCaseService;
    }

    @GetMapping("/getCatalogCase")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(catalogCaseService.getAll());
    }

    @GetMapping("/getCatalogCase/{id}")
    public ResponseEntity<?> getCatalogCaseById(@PathVariable int id){
        return ResponseEntity.ok(catalogCaseService.getCatalogCaseById(id));
    }

    @DeleteMapping("/deleteCatalogCase/{id}")
    public void deleteCatalogCase(@PathVariable long id){
        catalogCaseService.deleteById(id);
    }

    @PostMapping("/addCatalogCase")
    public ResponseEntity<?> createCatalogCase(@RequestBody CatalogCase catalogCase){
        return ResponseEntity.ok(catalogCaseService.createCatalogCase(catalogCase));
    }
}
