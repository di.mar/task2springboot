package kz.aitu.crud.controller;

import kz.aitu.crud.model.Nomenclature;
import kz.aitu.crud.model.Notification;
import kz.aitu.crud.service.NotificationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NotificationController {
    private NotificationService notificationService;

    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping("/getNotificationRouting")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(notificationService.getAll());
    }

    @GetMapping("/getNotification/{id}")
    public ResponseEntity<?> getNotificationById(@PathVariable int id){
        return ResponseEntity.ok(notificationService.getNotificationById(id));
    }

    @DeleteMapping("/deleteNotification/{id}")
    public void deleteNotification(@PathVariable long id){
        notificationService.deleteById(id);
    }

    @PostMapping("/addNotification")
    public ResponseEntity<?> createNotification(@RequestBody Notification notification){
        return ResponseEntity.ok(notificationService.createNotification(notification));
    }
}
