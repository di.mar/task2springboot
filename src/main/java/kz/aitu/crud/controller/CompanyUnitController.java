package kz.aitu.crud.controller;

import kz.aitu.crud.model.Company;
import kz.aitu.crud.model.CompanyUnit;
import kz.aitu.crud.service.CompanyService;
import kz.aitu.crud.service.CompanyUnitService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyUnitController {
    private CompanyUnitService companyUnitService;

    public CompanyUnitController(CompanyUnitService companyUnitService) {
        this.companyUnitService = companyUnitService;
    }

    @GetMapping("/getCompanyUnit")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(companyUnitService.getAll());
    }

    @GetMapping("/getCompanyUnit/{id}")
    public ResponseEntity<?> getCompanyUnitById(@PathVariable int id){
        return ResponseEntity.ok(companyUnitService.getCompanyUnitById(id));
    }

    @DeleteMapping("/deleteCompanyUnit/{id}")
    public void deleteCompanyUnit(@PathVariable long id){
        companyUnitService.deleteById(id);
    }

    @PostMapping("/addCompanyUnit")
    public ResponseEntity<?> createCompany(@RequestBody CompanyUnit companyUnit){
        return ResponseEntity.ok(companyUnitService.createCompanyUnit(companyUnit));
    }
}
