package kz.aitu.crud.controller;

import kz.aitu.crud.model.ActivityJournal;
import kz.aitu.crud.service.ActivityJournalService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ActivityJournalController {
    private ActivityJournalService activityJournalService;

    public ActivityJournalController(ActivityJournalService activityJournalService) {
        this.activityJournalService = activityJournalService;
    }

    @GetMapping("/getActivityJournal")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(activityJournalService.getAll());
    }

    @GetMapping("/getActivityJournal/{id}")
    public ResponseEntity<?> getActivityJournalById(@PathVariable int id){
        return ResponseEntity.ok(activityJournalService.getActivityJournalById(id));
    }

    @DeleteMapping("/deleteActivityJournal/{id}")
    public void deleteActivityJournal(@PathVariable long id){
        activityJournalService.deleteById(id);
    }

    @PostMapping("/addActivityJournal")
    public ResponseEntity<?> createActivityJournal(@RequestBody ActivityJournal activityJournal){
        return ResponseEntity.ok(activityJournalService.createActivityJournal(activityJournal));
    }

}
