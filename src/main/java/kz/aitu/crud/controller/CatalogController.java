package kz.aitu.crud.controller;

import kz.aitu.crud.model.Catalog;
import kz.aitu.crud.model.CatalogCase;
import kz.aitu.crud.service.CatalogCaseService;
import kz.aitu.crud.service.CatalogService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CatalogController {
    private CatalogService catalogService;

    public CatalogController(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    @GetMapping("/getCatalog")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(catalogService.getAll());
    }

    @GetMapping("/getCatalog/{id}")
    public ResponseEntity<?> getCatalogById(@PathVariable int id){
        return ResponseEntity.ok(catalogService.getCatalogById(id));
    }

    @DeleteMapping("/deleteCatalog/{id}")
    public void deleteCatalog(@PathVariable long id){
        catalogService.deleteById(id);
    }

    @PostMapping("/addCatalog")
    public ResponseEntity<?> createCatalog(@RequestBody Catalog catalog){
        return ResponseEntity.ok(catalogService.createCatalog(catalog));
    }
}
