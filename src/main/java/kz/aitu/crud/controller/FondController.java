package kz.aitu.crud.controller;

import kz.aitu.crud.model.File;
import kz.aitu.crud.model.Fond;
import kz.aitu.crud.service.FileService;
import kz.aitu.crud.service.FondService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FondController {
    private FondService fondService;

    public FondController(FondService fondService) {
        this.fondService = fondService;
    }

    @GetMapping("/getFond")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(fondService.getAll());
    }

    @GetMapping("/getFond/{id}")
    public ResponseEntity<?> getFondById(@PathVariable int id){
        return ResponseEntity.ok(fondService.getFondById(id));
    }

    @DeleteMapping("/deleteFond/{id}")
    public void deleteFond(@PathVariable long id){
        fondService.deleteById(id);
    }

    @PostMapping("/addFond")
    public ResponseEntity<?> createFond(@RequestBody Fond fond){
        return ResponseEntity.ok(fondService.createFond(fond));
    }
}
