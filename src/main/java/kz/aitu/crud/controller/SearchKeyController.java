package kz.aitu.crud.controller;

import kz.aitu.crud.model.SearchKey;
import kz.aitu.crud.model.SearchKeyRouting;
import kz.aitu.crud.service.SearchKeyRoutingService;
import kz.aitu.crud.service.SearchKeyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class SearchKeyController {
    private SearchKeyService searchKeyService;

    public SearchKeyController(SearchKeyService searchKeyService) {
        this.searchKeyService = searchKeyService;
    }

    @GetMapping("/getSearchKey")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(searchKeyService.getAll());
    }

    @GetMapping("/getSearchKey/{id}")
    public ResponseEntity<?> getSearchKeyById(@PathVariable int id){
        return ResponseEntity.ok(searchKeyService.getSearchKeyById(id));
    }

    @DeleteMapping("/deleteSearchKey/{id}")
    public void deleteSearchKey(@PathVariable long id){
        searchKeyService.deleteById(id);
    }

    @PostMapping("/addSearchKey")
    public ResponseEntity<?> createSearchKey(@RequestBody SearchKey searchKey){
        return ResponseEntity.ok(searchKeyService.createSearchKey(searchKey));
    }
}
