package kz.aitu.crud.controller;

import kz.aitu.crud.model.Record;
import kz.aitu.crud.model.Request;
import kz.aitu.crud.service.RecordService;
import kz.aitu.crud.service.RequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RequestController {
    private RequestService requestService;

    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }

    @GetMapping("/getRequest")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(requestService.getAll());
    }

    @GetMapping("/getRequest/{id}")
    public ResponseEntity<?> getRequestById(@PathVariable int id){
        return ResponseEntity.ok(requestService.getRequestById(id));
    }

    @DeleteMapping("/deleteRequest/{id}")
    public void deleteRequest(@PathVariable long id){
        requestService.deleteById(id);
    }

    @PostMapping("/addRequest")
    public ResponseEntity<?> createRecord(@RequestBody Request request){
        return ResponseEntity.ok(requestService.createRequest(request));
    }
}
