package kz.aitu.crud.controller;

import kz.aitu.crud.model.RequestStatusHistory;
import kz.aitu.crud.model.SearchKeyRouting;
import kz.aitu.crud.service.SearchKeyRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchKeyRoatingController {
    private SearchKeyRoutingService searchKeyRoutingService;

    public SearchKeyRoatingController(SearchKeyRoutingService searchKeyRoutingService) {
        this.searchKeyRoutingService = searchKeyRoutingService;
    }

    @GetMapping("/getSearchKeyRouting")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(searchKeyRoutingService.getAll());
    }

    @GetMapping("/getSearchKeyRouting/{id}")
    public ResponseEntity<?> getSearchKeyRoutingById(@PathVariable int id){
        return ResponseEntity.ok(searchKeyRoutingService.getSearchKeyRoutingById(id));
    }

    @DeleteMapping("/deleteSearchKeyRouting/{id}")
    public void deleteSearchKeyRouting(@PathVariable long id){
        searchKeyRoutingService.deleteById(id);
    }

    @PostMapping("/addSearchKeyRouting")
    public ResponseEntity<?> createSearchKeyRouting(@RequestBody SearchKeyRouting searchKeyRouting){
        return ResponseEntity.ok(searchKeyRoutingService.createSearchKeyRouting(searchKeyRouting));
    }
}
