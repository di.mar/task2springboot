package kz.aitu.crud.controller;

import kz.aitu.crud.model.SearchKey;
import kz.aitu.crud.model.Share;
import kz.aitu.crud.service.ShareService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ShareController {
    private ShareService shareService;

    public ShareController(ShareService shareService) {
        this.shareService = shareService;
    }

    @GetMapping("/getShare")
    public ResponseEntity<?> getAll( ){
        return ResponseEntity.ok(shareService.getAll());
    }

    @GetMapping("/getShare/{id}")
    public ResponseEntity<?> getShareById(@PathVariable int id){
        return ResponseEntity.ok(shareService.getShareById(id));
    }

    @DeleteMapping("/deleteShare/{id}")
    public void deleteShare(@PathVariable long id){
        shareService.deleteById(id);
    }

    @PostMapping("/addShare")
    public ResponseEntity<?> createShare(@RequestBody Share share){
        return ResponseEntity.ok(shareService.createShare(share));
    }


}
