package kz.aitu.crud.service;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.model.Case;
import kz.aitu.crud.repository.AuthRepository;
import kz.aitu.crud.repository.CaseRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CaseService {
    private final CaseRepository caseRepository;

    public CaseService(CaseRepository caseRepository) {
        this.caseRepository = caseRepository;
    }

    public List<Case> getAll(){
        return (List<Case>) caseRepository.findAll();
    }

    public Case getCaseById(long id){
        return caseRepository.findById(id);
    }

    public void deleteById(long id) {
        caseRepository.deleteById(id);
    }

    public Case createCase(Case cased){
        return caseRepository.save(cased);
    }
}
