package kz.aitu.crud.service;

import kz.aitu.crud.model.ActivityJournal;
import kz.aitu.crud.model.Auth;
import kz.aitu.crud.repository.ActivityJournalRepository;
import kz.aitu.crud.repository.AuthRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityJournalService {

    private final ActivityJournalRepository  activityJournalRepository;

    public ActivityJournalService(ActivityJournalRepository activityJournalRepository) {
        this.activityJournalRepository = activityJournalRepository;
    }

    public List<ActivityJournal> getAll(){
        return (List<ActivityJournal>) activityJournalRepository.findAll();
    }

    public ActivityJournal getActivityJournalById(long id){
        return activityJournalRepository.findById(id);
    }

    public void deleteById(long id) {
        activityJournalRepository.deleteById(id);
    }

    public ActivityJournal createActivityJournal(ActivityJournal activityJournal){
        return activityJournalRepository.save(activityJournal);
    }
}

