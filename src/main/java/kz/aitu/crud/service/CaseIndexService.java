package kz.aitu.crud.service;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.model.CaseIndex;
import kz.aitu.crud.repository.AuthRepository;
import kz.aitu.crud.repository.CaseIndexRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CaseIndexService {
    private final CaseIndexRepository caseIndexRepository;

    public CaseIndexService(CaseIndexRepository caseIndexRepository) {
        this.caseIndexRepository = caseIndexRepository;
    }

    public List<CaseIndex> getAll(){
        return (List<CaseIndex>) caseIndexRepository.findAll();
    }

    public CaseIndex getCaseIndexById(long id){
        return caseIndexRepository.findById(id);
    }

    public void deleteById(long id) {
        caseIndexRepository.deleteById(id);
    }

    public CaseIndex createCaseIndex(CaseIndex caseIndex){
        return caseIndexRepository.save(caseIndex);
    }
}
