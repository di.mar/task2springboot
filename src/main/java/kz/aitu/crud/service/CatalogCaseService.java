package kz.aitu.crud.service;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.model.CatalogCase;
import kz.aitu.crud.repository.AuthRepository;
import kz.aitu.crud.repository.CatalogCaseRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CatalogCaseService {
    private final CatalogCaseRepository catalogCaseRepository;

    public CatalogCaseService(CatalogCaseRepository catalogCaseRepository) {
        this.catalogCaseRepository = catalogCaseRepository;
    }

    public List<CatalogCase> getAll(){
        return (List<CatalogCase>) catalogCaseRepository.findAll();
    }

    public CatalogCase getCatalogCaseById(long id){
        return catalogCaseRepository.findById(id);
    }

    public void deleteById(long id) {
        catalogCaseRepository.deleteById(id);
    }

    public CatalogCase createCatalogCase(CatalogCase catalogCase){
        return catalogCaseRepository.save(catalogCase);
    }
}
