package kz.aitu.crud.service;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.model.Catalog;
import kz.aitu.crud.model.CatalogCase;
import kz.aitu.crud.repository.AuthRepository;
import kz.aitu.crud.repository.CatalogRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CatalogService {
    private final CatalogRepository catalogRepository;

    public CatalogService(CatalogRepository catalogRepository) {
        this.catalogRepository = catalogRepository;
    }

    public List<Catalog> getAll(){
        return (List<Catalog>)  catalogRepository.findAll();
    }

    public Catalog getCatalogById(long id){
        return catalogRepository.findById(id);
    }

    public void deleteById(long id) {
        catalogRepository.deleteById(id);
    }

    public Catalog createCatalog(Catalog catalog){
        return catalogRepository.save(catalog);
    }
}
