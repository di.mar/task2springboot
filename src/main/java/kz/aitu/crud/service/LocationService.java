package kz.aitu.crud.service;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.model.Location;
import kz.aitu.crud.repository.AuthRepository;
import kz.aitu.crud.repository.LocationRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationService {
    private final LocationRepository locationRepository;

    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public List<Location> getAll(){
        return (List<Location>) locationRepository.findAll();
    }

    public Location getLocationById(long id){
        return locationRepository.findById(id);
    }

    public void deleteById(long id) {
        locationRepository.deleteById(id);
    }

    public Location createLocation(Location location){
        return locationRepository.save(location);
    }
}
