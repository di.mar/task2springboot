package kz.aitu.crud.service;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.model.Notification;
import kz.aitu.crud.repository.AuthRepository;
import kz.aitu.crud.repository.NotificationRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class NotificationService {
    private final NotificationRepository notificationRepository;

    public NotificationService(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    public List<Notification> getAll(){
        return (List<Notification>) notificationRepository.findAll();
    }

    public Notification getNotificationById(long id){
        return notificationRepository.findById(id);
    }

    public void deleteById(long id) {
        notificationRepository.deleteById(id);
    }

    public Notification createNotification(Notification notification){
        return notificationRepository.save(notification);
    }
}
