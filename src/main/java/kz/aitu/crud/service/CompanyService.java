package kz.aitu.crud.service;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.model.Company;
import kz.aitu.crud.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {
    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<Company> getAll(){
        return (List<Company>) companyRepository.findAll();
    }

    public Company getCompanyById(long id){
        return companyRepository.findById(id);
    }

    public void deleteById(long id) {
        companyRepository.deleteById(id);
    }

    public Company createCompany(Company company){
        return companyRepository.save(company);
    }
}
