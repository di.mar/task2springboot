package kz.aitu.crud.service;

import kz.aitu.crud.model.SearchKeyRouting;
import kz.aitu.crud.model.Users;
import kz.aitu.crud.repository.SearchKeyRoutingRepository;
import kz.aitu.crud.repository.UsersRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersService {
    private final UsersRepository usersRepository;

    public UsersService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public List<Users> getAll(){
        return (List<Users>) usersRepository.findAll();
    }

    public Users getUsersById(long id){
        return usersRepository.findById(id);
    }

    public void deleteById(long id) {
        usersRepository.deleteById(id);
    }

    public Users createUsers(Users users){
        return usersRepository.save(users);
    }
}
