package kz.aitu.crud.service;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.model.Fond;
import kz.aitu.crud.repository.AuthRepository;
import kz.aitu.crud.repository.FondRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FondService {
    private final FondRepository fondRepository;

    public FondService(FondRepository fondRepository) {
        this.fondRepository = fondRepository;
    }

    public List<Fond> getAll(){
        return (List<Fond>) fondRepository.findAll();
    }

    public Fond getFondById(long id){
        return fondRepository.findById(id);
    }

    public void deleteById(long id) {
        fondRepository.deleteById(id);
    }

    public Fond createFond(Fond fond){
        return fondRepository.save(fond);
    }
}
