package kz.aitu.crud.service;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.model.RequestStatusHistory;
import kz.aitu.crud.repository.AuthRepository;
import kz.aitu.crud.repository.RequestStatusHistoryRepository;
import org.springframework.stereotype.Service;

import javax.persistence.SecondaryTable;
import java.util.List;
@Service
public class RequestStatusHistoryService {
    private final RequestStatusHistoryRepository requestStatusHistoryRepository;

    public RequestStatusHistoryService(RequestStatusHistoryRepository requestStatusHistoryRepository) {
        this.requestStatusHistoryRepository = requestStatusHistoryRepository;
    }

    public List<RequestStatusHistory> getAll(){
        return (List<RequestStatusHistory>) requestStatusHistoryRepository.findAll();
    }

    public RequestStatusHistory getRequestStatusById(long id){
        return requestStatusHistoryRepository.findById(id);
    }

    public void deleteById(long id) {
        requestStatusHistoryRepository.deleteById(id);
    }

    public RequestStatusHistory createRequestStatus(RequestStatusHistory requestStatusHistory){
        return requestStatusHistoryRepository.save(requestStatusHistory);
    }
}
