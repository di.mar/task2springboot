package kz.aitu.crud.service;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.model.CompanyUnit;
import kz.aitu.crud.repository.AuthRepository;
import kz.aitu.crud.repository.CompanyUnitRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyUnitService {
    private final CompanyUnitRepository companyUnitRepository;

    public CompanyUnitService(CompanyUnitRepository companyUnitRepository) {
        this.companyUnitRepository = companyUnitRepository;
    }

    public List<CompanyUnit> getAll(){
        return (List<CompanyUnit>) companyUnitRepository.findAll();
    }

    public CompanyUnit getCompanyUnitById(long id){
        return companyUnitRepository.findById(id);
    }

    public void deleteById(long id) {
        companyUnitRepository.deleteById(id);
    }

    public CompanyUnit createCompanyUnit(CompanyUnit companyUnit){
        return companyUnitRepository.save(companyUnit);
    }
}
