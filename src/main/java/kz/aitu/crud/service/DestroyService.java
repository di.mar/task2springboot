package kz.aitu.crud.service;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.model.Destroy;
import kz.aitu.crud.repository.AuthRepository;
import kz.aitu.crud.repository.DestroyRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class DestroyService {
    private final DestroyRepository destroyRepository;

    public DestroyService(DestroyRepository destroyRepository) {
        this.destroyRepository = destroyRepository;
    }

    public List<Destroy> getAll(){
        return (List<Destroy>) destroyRepository.findAll();
    }

    public Destroy getDestroyById(long id){
        return destroyRepository.findById(id);
    }

    public void deleteById(long id) {
        destroyRepository.deleteById(id);
    }

    public Destroy createDestroy(Destroy destroy){
        return destroyRepository.save(destroy);
    }
}
