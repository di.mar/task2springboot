package kz.aitu.crud.service;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.model.File;
import kz.aitu.crud.model.FileRouting;
import kz.aitu.crud.repository.AuthRepository;
import kz.aitu.crud.repository.FileRoutingRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileRoutingService {
    private final FileRoutingRepository fileRoutingRepository;

    public FileRoutingService(FileRoutingRepository fileRoutingRepository) {
        this.fileRoutingRepository = fileRoutingRepository;
    }

    public List<FileRouting> getAll(){
        return (List<FileRouting>) fileRoutingRepository.findAll();
    }

    public FileRouting getFileRoutingById(long id){
        return fileRoutingRepository.findById(id);
    }

    public void deleteById(long id) {
        fileRoutingRepository.deleteById(id);
    }

    public FileRouting createFileRouting(FileRouting fileRouting){
        return fileRoutingRepository.save(fileRouting);
    }
}
