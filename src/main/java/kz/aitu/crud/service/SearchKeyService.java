package kz.aitu.crud.service;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.model.SearchKey;
import kz.aitu.crud.repository.AuthRepository;
import kz.aitu.crud.repository.SearchKeyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchKeyService {
    private final SearchKeyRepository searchKeyRepository;

    public SearchKeyService(SearchKeyRepository searchKeyRepository) {
        this.searchKeyRepository = searchKeyRepository;
    }

    public List<SearchKey> getAll(){
        return (List<SearchKey>) searchKeyRepository.findAll();
    }

    public SearchKey getSearchKeyById(long id){
        return searchKeyRepository.findById(id);
    }

    public void deleteById(long id) {
        searchKeyRepository.deleteById(id);
    }

    public SearchKey createSearchKey(SearchKey searchKey){
        return searchKeyRepository.save(searchKey);
    }
}
