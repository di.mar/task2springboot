package kz.aitu.crud.service;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.model.Request;
import kz.aitu.crud.repository.AuthRepository;
import kz.aitu.crud.repository.RequestRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RequestService {
    private final RequestRepository requestRepository;

    public RequestService(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    public List<Request> getAll(){
        return (List<Request>) requestRepository.findAll();
    }

    public Request getRequestById(long id){
        return requestRepository.findById(id);
    }

    public void deleteById(long id) {
        requestRepository.deleteById(id);
    }

    public Request createRequest(Request request){
        return requestRepository.save(request);
    }
}
