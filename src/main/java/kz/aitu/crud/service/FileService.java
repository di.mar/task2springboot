package kz.aitu.crud.service;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.model.File;
import kz.aitu.crud.repository.AuthRepository;
import kz.aitu.crud.repository.FileRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileService {
    private final FileRepository fileRepository;

    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public List<File> getAll(){
        return (List<File>) fileRepository.findAll();
    }

    public File getFileById(long id){
        return fileRepository.findById(id);
    }

    public void deleteById(long id) {
        fileRepository.deleteById(id);
    }

    public File createFile(File file){
        return fileRepository.save(file);
    }
}
