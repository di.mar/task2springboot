package kz.aitu.crud.service;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.model.Record;
import kz.aitu.crud.repository.AuthRepository;
import kz.aitu.crud.repository.RecordRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RecordService {
    private final RecordRepository recordRepository;

    public RecordService(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }

    public List<Record> getAll(){
        return (List<Record>) recordRepository.findAll();
    }

    public Record getRecordById(long id){
        return recordRepository.findById(id);
    }

    public void deleteById(long id) {
        recordRepository.deleteById(id);
    }

    public Record createRecord(Record record){
        return recordRepository.save(record);
    }


}
