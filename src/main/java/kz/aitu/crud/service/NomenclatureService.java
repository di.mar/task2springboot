package kz.aitu.crud.service;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.model.Nomenclature;
import kz.aitu.crud.repository.AuthRepository;
import kz.aitu.crud.repository.NomenclatureRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NomenclatureService {
    private final NomenclatureRepository nomenclatureRepository;

    public NomenclatureService(NomenclatureRepository nomenclatureRepository) {
        this.nomenclatureRepository = nomenclatureRepository;
    }

    public List<Nomenclature> getAll(){
        return (List<Nomenclature>) nomenclatureRepository.findAll();
    }

    public Nomenclature getNomenclatureById(long id){
        return nomenclatureRepository.findById(id);
    }

    public void deleteById(long id) {
        nomenclatureRepository.deleteById(id);
    }

    public Nomenclature createNomenclature(Nomenclature nomenclature){
        return nomenclatureRepository.save(nomenclature);
    }
}
