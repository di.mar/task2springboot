package kz.aitu.crud.service;

import kz.aitu.crud.model.Auth;
import kz.aitu.crud.model.SearchKeyRouting;
import kz.aitu.crud.repository.AuthRepository;
import kz.aitu.crud.repository.SearchKeyRoutingRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchKeyRoutingService {
    private final SearchKeyRoutingRepository searchKeyRoutingRepository;

    public SearchKeyRoutingService(SearchKeyRoutingRepository searchKeyRoutingRepository) {
        this.searchKeyRoutingRepository = searchKeyRoutingRepository;
    }

    public List<SearchKeyRouting> getAll(){
        return (List<SearchKeyRouting>) searchKeyRoutingRepository.findAll();
    }

    public SearchKeyRouting getSearchKeyRoutingById(long id){
        return searchKeyRoutingRepository.findById(id);
    }

    public void deleteById(long id) {
        searchKeyRoutingRepository.deleteById(id);
    }

    public SearchKeyRouting createSearchKeyRouting(SearchKeyRouting searchKeyRouting){
        return searchKeyRoutingRepository.save(searchKeyRouting);
    }
}
