package kz.aitu.crud.service;

import kz.aitu.crud.model.Share;
import kz.aitu.crud.model.Tempfiles;
import kz.aitu.crud.repository.ShareRepository;
import kz.aitu.crud.repository.TempfilesRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TempfilesService {
    private final TempfilesRepository tempfilesRepository;

    public TempfilesService(TempfilesRepository tempfilesRepository) {
        this.tempfilesRepository = tempfilesRepository;
    }

    public List<Tempfiles> getAll(){
        return (List<Tempfiles>) tempfilesRepository.findAll();
    }

    public Tempfiles getTempfilesById(long id){
        return tempfilesRepository.findById(id);
    }

    public void deleteById(long id) {
        tempfilesRepository.deleteById(id);
    }

    public Tempfiles createTempfiles(Tempfiles tempfiles){
        return tempfilesRepository.save(tempfiles);
    }
}
